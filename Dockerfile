### vi Dockerfile
FROM openjdk:8u302-jdk-oraclelinux8

# copy jar file on to container
COPY spring-boot-journal-0.0.1-SNAPSHOT.jar /home/app.jar

WORKDIR /home
# RUN cd /home

CMD  [ "java", "-jar", "app.jar" ]


